<?php

use Illuminate\Support\Facades\Route;
use App\Models\Publication;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// $publications = [
//     [
//         'title' => 'Sensacja! Riot usunął popularną postać Yumi z Lola',
//         'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsum culpa minus quae nulla voluptatum? Dignissimos, voluptate corporis itaque dolores quod minima repudiandae exercitationem commodi, dolorum aliquam quia rerum quam cumque',
//         'author' => 'Riot company'
//     ],
//     [
//         'title' => 'Izak kończy z karierą na Twichu',
//         'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsum culpa minus quae nulla voluptatum? Dignissimos, voluptate corporis itaque dolores quod minima repudiandae exercitationem commodi, dolorum aliquam quia rerum quam cumque',
//         'author' => 'Twich'
//     ],
//     [
//         'title' => 'W SCI znowu tragiczny plan lekcji - władze szkoły szukają winowajcy',
//         'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsum culpa minus quae nulla voluptatum? Dignissimos, voluptate corporis itaque dolores quod minima repudiandae exercitationem commodi, dolorum aliquam quia rerum quam cumque',
//         'author' => 'samorzad',
//     ],
//     [
//         'title' => 'Pierwsza tesla z spalinowym silnikem v8, 4l',
//         'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsum culpa minus quae nulla voluptatum? Dignissimos, voluptate corporis itaque dolores quod minima repudiandae exercitationem commodi, dolorum aliquam quia rerum quam cumque',
//         'author' => 'samorząd sci',
//     ],
//     [
//         'title' => 'Polski rząd każe palić wszystkim tylko nie oponami',
//         'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsum culpa minus quae nulla voluptatum? Dignissimos, voluptate corporis itaque dolores quod minima repudiandae exercitationem commodi, dolorum aliquam quia rerum quam cumque',
//         'author' => 'Józef Piłsudzki',
//     ],
//     [
//         'title' => 'Uczeń sci oświadzcza że absolutnie nie umie laravela',
//         'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsum culpa minus quae nulla voluptatum? Dignissimos, voluptate corporis itaque dolores quod minima repudiandae exercitationem commodi, dolorum aliquam quia rerum quam cumque',
//         'author' => 'uczeń sci',
//     ],
//     [
//         'title' => 'Sensacja: Uczeń sci jednak umie laravela',
//         'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsum culpa minus quae nulla voluptatum? Dignissimos, voluptate corporis itaque dolores quod minima repudiandae exercitationem commodi, dolorum aliquam quia rerum quam cumque',
//         'author' => 'szczęśliwy uczeń sci',
//     ],
// ];

// $publications = Publication::all();
// Route::get('/posts', function () use ($publications) {
//     return view('posts', ['publications' => $publications]);
// })->name("posts");

Route::get('posts', [PublicationController::class, 'index'])->name('posts-view');

// Route::get('/post/{index}', function (int $id) use ($publications) {
//     if( $id >= count($publications) || $id <0)
//     {
//         abort(404);
//     }
//     return view('post', ['post' => $publications[$id]]);
// })->name('post');

Route::post('comment', [CommentController::class, 'store'])->name('comment.store');
Route::get('single_post/{id}', [PublicationController::class, 'show'])->name('post.view');

Route::post('post', [PublicationController::class, 'store'])->name('posts.store');
Route::get('post/create', [PublicationController::class, 'create'])->name('posts.create');

Route::get('comment/create', [CommentController::class, 'create'])->name('comment.create');
Route::post('comment/{publication}', [CommentController::class, 'update'])->name('comment.update');



Route::post('post/{publication}', [PublicationController::class, 'update'])->name('posts.update');
Route::get('post/{publication}/edit', [PublicationController::class, 'edit'])->name('posts.edit')->middleware('auth');

Route::delete('post/{publication}', [PublicationController::class,
'destroy'])->name('posts-delete')->middleware('auth');


Route::get('login', [LoginController::class, 'index'])->name('login');

Route::post('login', [LoginController::class, 'login'])->name('auth.login');

Route::post('logout', [LoginController::class, 'logout'])->name('auth.logout');
