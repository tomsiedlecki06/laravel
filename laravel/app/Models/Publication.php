<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Publication extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'content',
        'author_id'
    ];
    public function author()
    {
        return $this->belongsTo(User::class,
        'author_id')->withTrashed();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'publication_id');
    }


    
}
