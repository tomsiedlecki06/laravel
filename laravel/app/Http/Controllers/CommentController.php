<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\User;



class CommentController extends Controller
{
    public function create()
    {
        return view('comment');
    }

    public function store(Request $request)
    {
        //dd($request['id']);
        $data = $request->validate([
			'content' => 'required|string',
            'author_id' => 'required|int'
		]);
        dd($data);
        $newComment = new Comment($data);
        $newComment->publication_id = $request["publication"];
        $newComment->save();

        // return redirect()->route('posts-view', [
        //     'publication' => $newPost->id
        // ])->with('success', 'Pomyślnie dodano publikację');
    }
}
