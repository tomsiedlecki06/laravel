<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth');
    }

	public function login(Request $request)
	{
		$data = $request->validate([
			'login' => 'required|email',
			'password' => 'required|string'
		]);
		$maciakla = Auth::attempt(['email' => $data['login'], 'password' => $data['password']]);
		echo $maciakla;
		if ($maciakla) {
			$request->session()->regenerate();
			return redirect()->intended('/posts');
		}
		return back()->withErrors([
			'login' => 'Login lub hasło nie sa poprawne.'
		]);
	}

	
	public function logout(Request $request)
	{
		Auth::logout();
		$request->session()->invalidate();
		$request->session()->regenerateToken();
		return redirect('/posts');
	}


}
?>
