<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Publication;
use App\Models\User;
use App\Http\Requests\PublicationRequest;
use App\Models\Comment;


class PublicationController extends Controller
{
    public function index()
    {
        $publications = Publication::paginate(5);
        // ! zapytac o to!!!!!!!!!!!!!!!!!!!!!!
        // return view('posts', [
        //     'publications' => $publications,
        //     'comments' => $publications[0]->comments
        // ]);
        return view('posts', [
                'publications' => $publications
            ]);
    }

    public function show(int $id)
    {
        // Znajdź modele o wskazanym ID i zwróć tylko jeden
        $publication = Publication::where('id', $id)->first();

        // Jak model jest null lub nie ustawiony to rzuć błędem
        if (empty($publication)) {
            abort(404);
        }

        return view('post', [
            'publication' => $publication,
            'comments' => $publication->comments
        ]);
    }

    public function create()
    {
        $users = User::all();
        return view('form', [
            'users' => $users,
        ]);
    }

    public function store(PublicationRequest $request)
    {
        $data = $request->validated();
        $newPost = new Publication($data);
        $newPost->author_id = $request->user()->id;
        $newPost->save();

        return redirect()->route('posts-view', [
            'publication' => $newPost->id
        ])->with('success', 'Pomyślnie dodano publikację');
    }

    public function edit(Request $request,Publication $publication)
    {
        if ($request->user()->id != $publication->author_id) 
        {
    		abort(403);
	    }
        //ZAPYTAJ
        $users = User::all();
        //dd($users);

        return view('form', [
            'users' => $users,
            'post' => $publication
            
        ]);
    }

    public function update(Request $request,Publication $publication)
    {
        if ($request->user()->id != $publication->author_id) 
        {
    		abort(403);
	    }
        $data = $request->validate([
			'title' => 'required|string',
			'content' => 'required|string'
		]);
        
        $publication->fill($data);
        $publication->save();

        return redirect()->route('posts-view', [
            'publication' => $publication->id
        ])->with('success', 'Zmiany zapisane');
    }

    public function destroy(Publication $publication)
    {
        if ($request->user()->id != $publication->author_id) 
        {
    		abort(403);
	    }
        $publication->comments()->delete();
        $publication->delete();
        
        return redirect()->route('posts');
    }
    

}
