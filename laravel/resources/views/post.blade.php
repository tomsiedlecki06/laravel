@extends('layouts.app')
@section('content')
<section class="flex flex-col bg-teal-100 mt-50">
<p class ='bg-teal-300 font-bold mb-5'> Tytul: {{ $publication['title'] }}</p>
<p class ='bg-teal-400'>{{ $publication['content'] }}</p>
<p class ='bg-teal-500'>Autor: {{ $publication->author->name; }}</p>
<p class ='bg-slate-200 font-bold'>Komentarze: </p>
@auth
@if (auth()->user()->id == $publication->author->id)

        <a href="{{ route('posts.edit', ['publication' => $publication->id]) }}">Edytuj</a>
        <form action="{{ route('posts-delete', ['publication' => $publication->id]) }}"
            method="POST">
            @csrf
                    @method('DELETE')
                    <button type="submit">Usuń</button>
        </form>
        <form action="{{route('comment.create', ['publication' => $publication->id])}}">
                <input type="hidden" name='author_id' value={{auth()->user()->id}}>
        @csrf
        <button type="submit">dodaj komentarz</button>
        </from>
@endif
@endauth
        @foreach($comments as $comment)
        <p class ='bg-slate-200'><b>Autor:</b> {{ $comment->author['name'] }}</p>
        <p class ='bg-slate-200'>{{ $comment['content'] }}</p>
        <p class ='bg-slate-200'><b>Dodano:</b> {{ $comment['created_at'] }}</p>
        <p class ='bg-slate-200'>----------------------------------------</p>
        @endforeach
    </section>
@endsection
