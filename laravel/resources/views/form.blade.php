@extends('layouts.app')

@php
    $action = route('posts.store');
    $title = null;
    $content = null;
    $author_id = null;

    if (isset($post)) {
        $action = route('posts.update', ['publication' => $post->id]);
        $title = $post->title;
        $content = $post->content;
        $author_id = $post->author_id;
    }
@endphp


@section('content')

<div>
    {{ isset($post) ? 'Edycja' : 'Nowa publikacja' }}
</div>

<form action="{{ $action }}" method="POST">
    @csrf
    <input id="title" name="title" type="text" placeholder="Tytuł" value="{{ $title }}">
    @error('title')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
@enderror

<textarea name="content" id="content" placeholder="Treść..." cols="30" rows="10">{{ $content }}</textarea>
    @error('content')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
@enderror
    <input class="bg-red-300" type="submit" value="Zapisz">
</form>

@endsection
