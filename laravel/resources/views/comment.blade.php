@extends('layouts.app')

@php
    $action = route('comment.store');
    $content = null;
@endphp


@section('content')

<form action="{{ $action }}" method="POST">
    @csrf
    <input id="content" name="content" type="textarea" placeholder="content" value="{{ $content }}">
    @error('content')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>  
@enderror  
    <input class="bg-red-300" type="submit" value="wyslij">
</form>

@endsection
