@extends('layouts.app')

@section('content')


@auth
	<div class="flex">
    	<h1>Witaj <strong>{{ Auth::user()->name }}</strong>!</h1>
	    <form action="{{ route('auth.logout') }}" method="POST">
        	@csrf
        	<div class="flex items-center justify-between">
            	<input type="submit" value="Wyloguj" />
        	</div>
    	</form>
	</div>
@else
	<a href="{{ route('login') }}">Logowanie</a>
@endauth


    @if (isset($publications))
    @for($i=0;$i< sizeof($publications);$i++)
    
    <section class="flex flex-col bg-teal-100 my-20 drop-shadow-2xl rounded-md">
        @if($i == 0)
            <p class="text-4xl">Najnowszy artykuł: </p> <br><br>
        @endif
        <a href="single_post/{{$publications[$i]['id']}}">

            <p class ="bg-teal-300 font-bold mb-5">{{$publications[$i]['title']}}</p>
            <p class ="bg-teal-400">{{$publications[$i]['content']}}</p><br>
            <p class ="bg-teal-500">Autor: {{$publications[$i]->author->name;}}</p><br>
        </a>
        <p class ='bg-slate-200 font-bold'>Komentarze: </p>
        @foreach($publications[$i]->comments as $comment)
            <p class ='bg-slate-200'><b>Autor:</b> {{ $comment->author['name'] }}</p>
            <p class ='bg-slate-200'>{{ $comment['content'] }}</p>
            <p class ='bg-slate-200'><b>Dodano:</b> {{ $comment['created_at'] }}</p>
            <p class ='bg-slate-200'>----------------------------------------</p>
        @endforeach

    </section>
    
        
    @endfor
    {{ $publications->links() }}
    @endif
@endsection
