@extends('layouts.app')

@php
    $action = route('login');
    $login = null;
    $password = null;
@endphp


@section('content')

<form action="{{ $action }}" method="POST">
    @csrf
    <input id="login" name="login" type="text" placeholder="login" value="{{ $login }}">
    @error('login')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
@enderror

<input id="password" name="password" type="text" placeholder="password" value="{{ $password }}">
    @error('password')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
@enderror
    <input class="bg-red-300" type="submit" value="Zapisz">
</form>

@endsection
