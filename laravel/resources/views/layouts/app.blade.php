<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Document</title>
</head>
<body class = "font-mono text-2xl">
    @yield('content')
    
</body>
</html>